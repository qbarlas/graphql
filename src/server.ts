
require('dotenv').config();

import * as express from 'express';
import { graphqlExpress } from 'apollo-server-express';
import { createApolloFetch } from 'apollo-fetch';
import { makeRemoteExecutableSchema, mergeSchemas, introspectSchema } from 'graphql-tools';
import * as bodyParser from 'body-parser';
// import { localSchema } from './graphql-schema';

async function run() {

  const app = express();

  const createRemoteSchema = async (uri: string) => {
  	const fetcher = createApolloFetch({uri});
  	return makeRemoteExecutableSchema({
  		schema: await introspectSchema(fetcher),
  		fetcher
  	});
  };
  const api1 = await createRemoteSchema('http://localhost:4001/graphql');
  const api2 = await createRemoteSchema('http://localhost:4002/graphql');
  // const linkSchemaDefs = `
  // extend type Event {
  //   location: Location
  // }
  // `;

  const schema = mergeSchemas({
    schemas: [/*localSchema,*/ api1, api2/*, linkSchemaDefs*/],
    resolvers: mergeInfo => ({/*
      Event: {
        location: {
          fragment: `fragment EventFragment on Event {cityName}`,
          resolve(parent: any, args: any, context: any, info: any) {
            const place: string = parent.cityName;
            return mergeInfo.delegate( 'query', 'location', {place}, context, info);
          }
        }
      }*/
    })
  });

  app.use('/graphql',  bodyParser.json(), graphqlExpress(request => {
    const graphqlConfig = {
      schema: schema
    };

    if(process.env.NODE_ENV === 'development'){
      const startTime = Date.now();
      // Enable graphiql
      graphqlConfig.graphiql = process.env.NODE_ENV === 'development';

      graphqlConfig.formatError = error => ({
        message: error.message,
        locations: error.locations,
        stack: error.stack ? error.stack.split('\n') : [],
        path: error.path
      });
      graphqlConfig.extensions = function({ document, variables, operationName, result }) {
        return { runTime: Date.now() - startTime };
      };
    }
    return graphqlConfig;
   })
  );

  console.log('Starting server. Env is ' + process.env.NODE_ENV);

  app.listen(process.env.NODE_PORT || 4000);
}

run();
