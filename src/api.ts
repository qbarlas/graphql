
require('dotenv').config();

import * as express from 'express';
import * as bodyParser from 'body-parser';
import { graphqlExpress } from 'apollo-server-express';

import {
  graphql,
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString
} from 'graphql';

const apiSchema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
      api1 : {
        type: GraphQLString,
        resolve() {
          return 'world';
        }
      }
    }
  })
});
const apiSchema2 = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
      api2 : {
        type: GraphQLString,
        resolve() {
          return 'world';
        }
      }
    }
  })
});

function startApi(schema : GraphQLSchema, port: int){
  const app = express();

  app.use('/graphql', bodyParser.json(), graphqlExpress( request => {
    console.log('%s %s', request.method, request.originalUrl, request.body);
    return { schema: schema };
  }));
  console.log('Starting API on port ' + port);
  app.listen(port);
}

startApi(apiSchema,4001);
startApi(apiSchema2,4002);
